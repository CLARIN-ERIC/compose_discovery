describe('Frontpage', () => {
    it('renders all elements', () => {
        cy.visit('/');
        cy.contains('div', 'clarin.eu website account').should('be.visible');
    })
})